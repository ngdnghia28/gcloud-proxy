var express = require('express')
var proxy = require('http-proxy-middleware')

var app = express()

app.use('/', proxy({
  target: 'https://www.googleapis.com/',
  changeOrigin: true,
  onProxyReq
}))

app.listen(3000)

function onProxyReq(proxyReq, req, res) {
  proxyReq.setHeader('Authorization', 'Bearer ya29.GlxjB48b2e428AHjo5dc0P5qbDS4c4EBFw_PvYs44YHXfIIAjedUFd3kuNHd2IAlzIhwC6NBT5PNzOtcEUJ0HKrbgWbGJoInVCR5N_ArFkJ9_FzattTetlqn8ebV4Q')
}
