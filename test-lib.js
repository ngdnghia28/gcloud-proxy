const { google } = require('googleapis');


(async () => {
  const oAuth2Client = new google.auth.OAuth2(
    'asdsad', 'dasda'
  );
  oAuth2Client.credentials = { refresh_token: 'refreshToken' }
  const analytics = google.analytics({
    version: 'v3',
  });
  try {
    const res = await analytics.management.accountSummaries.list(null, { rootUrl: 'http://localhost:3000' });
    console.log(JSON.stringify(res.data, null, 2));
  } catch (e) {
    console.log(e)
  }
})()
